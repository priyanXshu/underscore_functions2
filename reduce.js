const reduce = (items, cb, startValue) => {
  let startIdx = 0;
  let initialValue = startValue !== undefined ? startValue : items[0];
  for (let i = startIdx; i < items.length; i++) {
    initialValue = cb(initialValue, items[i]);
  }
  return initialValue;
};

module.exports = reduce;
