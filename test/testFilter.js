const filter = require("../filter");
const items = [1, 2, 3, 4, 5, 5];

const cb = (num) => {
  return num > 2;
};

console.log(filter(items, cb));
