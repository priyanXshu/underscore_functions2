const each = require("../each");
const items = [1, 2, 3, 4, 5, 5];

const cb = (items, i) => {
  console.log(`${items} is at ${i}`);
};

each(items, cb);
