const reduce = require("../reduce");
const items = [1, 2, 3, 4, 5, 5];

const cb = (initialValue, currentValue) => {
  return initialValue + currentValue;
};

console.log(reduce(items, cb, 0));
