const find = require("../find");
const items = [1, 2, 3, 4, 5, 5];

const cb = (num) => {
  return num > 4;
};

console.log(find(items, cb));
