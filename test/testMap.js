const map = require("../map");
const items = [1, 2, 3, 4, 5, 5];

const cb = (items) => {
  return items * 10;
};

console.log(map(items, cb));
