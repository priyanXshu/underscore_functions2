const filter = (items, cb) => {
  let filteredArray = [];
  for (let i = 0; i < items.length; i++) {
    if (cb(items[i])) {
      filteredArray.push(items[i]);
    }
  }
  return filteredArray;
};

module.exports = filter;
