const flatten = (nestedArray) => {
  const flatArray = [];
  const recursion = (arr) => {
    for (let i = 0; i < arr.length; i++) {
      if (Array.isArray(arr[i])) {
        recursion(arr[i]);
      } else {
        flatArray.push(arr[i]);
      }
    }
  };
  recursion(nestedArray)
  return flatArray;
};

module.exports = flatten;
