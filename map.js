const map = (items, cb) => {
  const resMap = [];
  for (let i = 0; i < items.length; i++) {
    resMap.push(cb(items[i]));
  }
  return resMap;
};

module.exports = map;
